# refactor

Мета цього проєкта -- створити програму, котра зможе знаходити та усувати антипатерни у коді.

## Використання
### Вимоги
Для збірки потребується Gradle 6.8.
Для коректної роботи програми на великих проєктах може потребуватися до 32 ГБ ОЗУ.
### Команди
* Збірка: `gradle build` або `./build.sh`
* Виготовлення JAR-файлу: `gradle jar` або `./jar.sh`
* Виконання: `gradle run` або `./run.sh` або `java -jar refactor-XXX.jar` (для JAR-файлу, що вже зібраний)


Icons made by <a href="https://www.flaticon.com/authors/flat-icons" title="Flat Icons">Flat Icons</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com </a>